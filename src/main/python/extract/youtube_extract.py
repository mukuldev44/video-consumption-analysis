from apiclient.discovery import build
from apiclient.errors import HttpError
import requests
import json
import pandas as pd
import os

DEVELOPER_KEY = "AIzaSyClGXhNY9UoZUCjlYdvXk5zWsvGz5UdT5s"


def youtube_search(vid):
    """
    Call the search. list method to retrieve results matching the specified query term.
    :param vid:
    :return:
    """
    youtube_api_service_name = "youtube"
    youtube_api_version = "v3"
    youtube = build(youtube_api_service_name, youtube_api_version, developerKey=DEVELOPER_KEY)
    search_response = youtube.search().list(
    part="snippet",
    relatedToVideoId=vid,
    type="video",
    ).execute()
    return search_response


def youtube_search_requests(vid):
    DEVELOPER_KEY = "AIzaSyClGXhNY9UoZUCjlYdvXk5zWsvGz5UdT5s"
    url = "https://www.googleapis.com/youtube/v3/videos?part=snippet&id={id}&key={api_key}"
    r = requests.get(url.format(id=vid, api_key=DEVELOPER_KEY))
    return r.json()


def get_csv():
    cwd = os.getcwd()
    out_dir = os.path.join(cwd, "resources/persist")


if __name__ == '__main__':
    try:
        search2 = youtube_search_requests("DMvHONEyH5A")
        print search2
        print json.dumps(search2)
    except HttpError as  e:
        print "An HTTP error %d occurred:\n%s" % (e.resp.status, e.content)