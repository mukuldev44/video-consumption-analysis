package com.embibe.dsl.videoconsumptionanalysis.executor

import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Dataset, SaveMode, SparkSession}

object VideoClickDurationAnalytics {
  def main(args: Array[String]): Unit = {
    // SparkSession
    val spark = SparkSession.builder
      .master("yarn-client")
      .appName(getClass.getSimpleName)
      .enableHiveSupport()
      .getOrCreate()
    import spark.implicits._

    // ---- Analytics ----
    val intermediateClickStreamPath = "s3a://dsl-datastore/data/video-analysis/clickstream-learningmap/year/2018/"
    val clickstreamDS = spark.read.parquet(intermediateClickStreamPath).as[UrlMappedSchema]
      .filter(row => row.year == 2018 && row.month == 7)
    val videoDS = getVideoClickStreamDF(clickstreamDS)
    val pageLoadDS = getPageLoadClickStreamDF(clickstreamDS)
    analysis(videoDS, pageLoadDS)
  }

  def getVideoClickStreamDF(urlMappedDS: Dataset[UrlMappedSchema]) = {
    urlMappedDS
      .filter(trim(col("event_name")) === lit("play/pause video"))
  }

  def getPageLoadClickStreamDF(urlMappedDS: Dataset[UrlMappedSchema]) = {
    urlMappedDS.filter(trim(col("event_name")) === lit("page load"))
  }

  def analysis(videoDS: Dataset[UrlMappedSchema], pageLoadDS: Dataset[UrlMappedSchema]) = {
    val videoPlayDS = videoDS.filter(u => u.video_state == "play")
    val analysisOutputPrefixPath = "s3a://dsl-datastore/data/video-analysis/output"
    val countAggregationPath = analysisOutputPrefixPath + "/count-aggregation/year/2018"

    val conceptPageLoadDS = getAggregateCountDataSet(pageLoadDS, "concept_code", "concept_name", "concept")
    val conceptVideoDS = getAggregateCountDataSet(videoPlayDS, "concept_code", "concept_name", "concept")
    val conceptCountAggDS = getVideoPageLoadJoinedDS(conceptPageLoadDS, conceptVideoDS, "count-aggregation", "concept")
    conceptCountAggDS.coalesce(1).write.option("header", "true").mode(SaveMode.Append).csv(countAggregationPath)

    //    val subjectPageLoadDS = getAggregateCountDataSet(pageLoadDS, "subject_code", "subject_name", "subject")
    //    val subjectVideoDS = getAggregateCountDataSet(videoPlayDS, "subject_code", "subject_name", "subject")
    //    val subjectCountAggDS = getVideoPageLoadJoinedDS(subjectPageLoadDS, subjectVideoDS, "count-aggregation", "subject")
    //    subjectCountAggDS.coalesce(1).write.option("header", "true").mode(SaveMode.Append)
    //      .csv(countAggregationPath)
    //
    //    val chapterPageLoadDS = getAggregateCountDataSet(pageLoadDS, "chapter_code", "chapter_name", "chapter")
    //    val chapterVideoDS = getAggregateCountDataSet(videoPlayDS, "chapter_code", "chapter_name", "chapter")
    //    val chapterCountAggDS = getVideoPageLoadJoinedDS(chapterPageLoadDS, chapterVideoDS, "count-aggregation", "chapter")
    //    chapterCountAggDS.coalesce(1).write.option("header", "true").mode(SaveMode.Append)
    //      .csv(countAggregationPath)
    //
    //    val allPageLoadDS = getAggregateCountDataSet(pageLoadDS, "", "", "all")
    //    val allVideoDS = getAggregateCountDataSet(videoPlayDS, "", "", "all")
    //    val allCountAggDS = getVideoPageLoadJoinedDS(allPageLoadDS, allVideoDS, "count-aggregation", "all")
    //    allCountAggDS.coalesce(1).write.option("header", "true").mode(SaveMode.Append)
    //      .csv(countAggregationPath)

    // ---- Video Duration Analysis ----
    val videoDurationAggregationPath = analysisOutputPrefixPath + "/video-duration-aggregation/year/2018"

    val lastClickStreamSessionDS = getLastClickStreamSessionDS(videoDS)
    val conceptVideoDurationAggDS = getVideoDurationAggregationDS(lastClickStreamSessionDS, "concept_code", "concept_name", "video-duration-aggregation", "concept")
    conceptVideoDurationAggDS.coalesce(1).write.option("header", "true").mode(SaveMode.Append).csv(videoDurationAggregationPath)
    //
    //    val subjectVideoDurationAggDS = getVideoDurationAggregationDS(lastClickStreamSessionDS, "subject_code", "subject_name", "video-duration-aggregation", "subject")
    //    subjectVideoDurationAggDS.coalesce(1).write.option("header", "true").mode(SaveMode.Append)
    //      .csv(videoDurationAggregationPath)
    //
    //    val chapterVideoDurationAggDS = getVideoDurationAggregationDS(lastClickStreamSessionDS, "chapter_code", "chapter_name", "video-duration-aggregation", "chapter")
    //    chapterVideoDurationAggDS.coalesce(1).write.option("header", "true").mode(SaveMode.Append)
    //      .csv(videoDurationAggregationPath)
    //
    //    val allVideoDurationAggDS = getVideoDurationAggregationDS(lastClickStreamSessionDS, "", "", "video-duration-aggregation", "all")
    //    allVideoDurationAggDS.coalesce(1).write.option("header", "true").mode(SaveMode.Append)
    //      .csv(videoDurationAggregationPath)
  }

  def getLastClickStreamSessionDS(ds: Dataset[UrlMappedSchema]) = {
    import ds.sparkSession.implicits._

    val lastClickStreamSessionDS = ds
      .groupByKey(x => (x.user_id, x.url, x.session_id, x.video_url))
      .reduceGroups((x, y) => if(x.timestamp > y.timestamp) x else y)
      .map(_._2)

    lastClickStreamSessionDS
  }

  def getVideoDurationAggregationDS(ds: Dataset[UrlMappedSchema], codeColumn: String, nameColumn: String, analysisType: String, analysisLevel: String) = {
    if (analysisLevel.toLowerCase().trim == "all") {
      ds
        .agg(sum(col("video_time_position")).as("total_duration"))
        .select(lit(analysisType).as("analysis_type"), lit(analysisLevel).as("analysis_level"), lit(codeColumn).as("code"), lit(nameColumn).as("name"), col("total_duration"))
    } else {
      ds
        .filter(col(codeColumn).isNotNull && col(codeColumn) =!= "")
        .groupBy(codeColumn, nameColumn)
        .agg(sum(col("video_time_position")).as("total_duration"))
        .select(lit(analysisType).as("analysis_type"), lit(analysisLevel).as("analysis_level"), col(codeColumn).as("code"), col(nameColumn).as("name"), col("total_duration"))
    }
  }

  def getVideoPageLoadJoinedDS(pageLoadDS: Dataset[AggregateCountSchema], videoDS: Dataset[AggregateCountSchema], analysisType: String, analysisLevel: String) = {
    import pageLoadDS.sparkSession.implicits._
    pageLoadDS
      .joinWith(videoDS, pageLoadDS("codeColumn") === videoDS("codeColumn"), "inner")
      .map { x =>
        val pageLoad = x._1
        val video = x._2
        val pageLoadCount = pageLoad.countColumn

        if(video != null){ // only for outer join
          val videoCount = video.countColumn
          val percentage = (videoCount.toDouble * 100) / pageLoadCount

          (analysisType, analysisLevel, pageLoad.codeColumn, pageLoad.nameColumn, pageLoadCount, videoCount, percentage)
        }
        else {
          (analysisType, analysisLevel, pageLoad.codeColumn, pageLoad.nameColumn, pageLoadCount, 0L, 0.0)
        }
      }.toDF("analysis_type", "analysis_level", "code", "name", "page_load_count", "video_play_count", "percentage")
  }

  def getAggregateCountDataSet(ds: Dataset[UrlMappedSchema], codeColumn: String, nameColumn: String, analysisLevel: String) = {
    import ds.sparkSession.implicits._
    if (analysisLevel.toLowerCase().trim == "all") {
      ds
        .agg(count("*").as("countColumn"))
        .select(lit("").as("codeColumn"), lit("").as("nameColumn"), col("countColumn"))
        .as[AggregateCountSchema]
    }
    else {
      ds
        .filter(col(codeColumn).isNotNull && col(codeColumn) =!= "")
        .groupBy(codeColumn, nameColumn)
        .agg(count(codeColumn).as("countColumn"))
        .select(col(codeColumn).as("codeColumn"), col(nameColumn).as("nameColumn"), col("countColumn"))
        .as[AggregateCountSchema]
    }
  }
}
