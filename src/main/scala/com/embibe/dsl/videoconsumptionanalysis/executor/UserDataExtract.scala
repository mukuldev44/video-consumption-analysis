package com.embibe.dsl.videoconsumptionanalysis.executor

import java.sql.Date

import org.apache.spark.sql.{DataFrame, Dataset, SaveMode, SparkSession}
import org.apache.spark.sql.expressions.{UserDefinedFunction, Window}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{ArrayType, StringType, StructField, StructType}

object UserDataExtract {
  def main(args: Array[String]): Unit = {
    // SparkSession
    val spark = SparkSession.builder
      .master("yarn-client")
      .appName(getClass.getSimpleName)
      .enableHiveSupport()
      .getOrCreate()
    import spark.implicits._

    val s3BucketName = "dsl-datastore"
    val govPath = s"s3a://$s3BucketName/" + "data/oneview/gov/daily/2018/11/30/dGOV_2018-11-30.json.gz"
    val govDF = spark.read.schema(govInputSchema).json(govPath)
      .filter(col("started_at") >= lit("2018-01-01"))
    govDF.repartition(400)
    val govDS = getGovInputDS(govDF)
    val trimGovDS = getGovAggregateDS_1(govDS)

    trimGovDS.write.mode(SaveMode.Overwrite).parquet("s3a://dsl-datastore/data/video-analysis/user-gov/year/2018/")
  }

  def getGovAggregateDS(ds: Dataset[GovInputSchema]) = {
    val partitionWindow = Window
      .partitionBy(col("user_id"), col("year"), col("month"), col("day"))
      .orderBy(desc("started_at"))

    ds.withColumn("rn", row_number().over(partitionWindow)).filter(col("rn") === 1).drop("rn")
  }

  /** More Optimized way, using DataSet Api
    *
    * @param ds
    * @return
    */
  def getGovAggregateDS_1(ds: Dataset[GovInputSchema]) = {
    import ds.sparkSession.implicits._

    ds
      .groupByKey(x => (x.user_id, x.year, x.month, x.month_day))
      .reduceGroups((x, y) => if(x.started_at > y.started_at) x else y)
      .map(_._2)
  }

  def getGovInputDS(df: DataFrame): Dataset[GovInputSchema] = {
    val pcAnonFunc = {(bitsat: Seq[String], gujarat_cet: Seq[String], jee_main: Seq[String], ibps_clerk_mains: Seq[String], mhcet: Seq[String], foundation_09: Seq[String], foundation_08: Seq[String], mht_cet: Seq[String], sbi_po_prelims: Seq[String], ts_eamcet: Seq[String], aiims: Seq[String], mh_cet: Seq[String], sbi_po_mains: Seq[String], ap_eamcet: Seq[String], foundation_10: Seq[String], jee_advanced: Seq[String], jipmer: Seq[String], neet: Seq[String], jee_hindi: Seq[String], sbi_clerk_mains: Seq[String], sbi_clerk_prelims: Seq[String], assam_cee: Seq[String]) =>
      val bitsat_2nd = if(bitsat != null) Some(bitsat(1)) else None
      val gujarat_cet_2nd = if(gujarat_cet != null) Some(gujarat_cet(1)) else None
      val jee_main_2nd = if(jee_main != null) Some(jee_main(1)) else None
      val ibps_clerk_mains_2nd = if(ibps_clerk_mains != null) Some(ibps_clerk_mains(1)) else None
      val mhcet_2nd = if(mhcet != null) Some(mhcet(1)) else None
      val foundation_09_2nd = if(foundation_09 != null) Some(foundation_09(1)) else None
      val foundation_08_2nd = if(foundation_08 != null) Some(foundation_08(1)) else None
      val mht_cet_2nd = if(mht_cet != null) Some(mht_cet(1)) else None
      val sbi_po_prelims_2nd = if(sbi_po_prelims != null) Some(sbi_po_prelims(1)) else None
      val ts_eamcet_2nd = if(ts_eamcet != null) Some(ts_eamcet(1)) else None
      val aiims_2nd = if(aiims != null) Some(aiims(1)) else None
      val mh_cet_2nd = if(mh_cet != null) Some(mh_cet(1)) else None
      val sbi_po_mains_2nd = if(sbi_po_mains != null) Some(sbi_po_mains(1)) else None
      val ap_eamcet_2nd = if(ap_eamcet != null) Some(ap_eamcet(1)) else None
      val foundation_10_2nd = if(foundation_10 != null) Some(foundation_10(1)) else None
      val jee_advanced_2nd = if(jee_advanced != null) Some(jee_advanced(1)) else None
      val jipmer_2nd = if(jipmer != null) Some(jipmer(1)) else None
      val neet_2nd = if(neet != null) Some(neet(1)) else None
      val jee_hindi_2nd = if(jee_hindi != null) Some(jee_hindi(1)) else None
      val sbi_clerk_mains_2nd = if(sbi_clerk_mains != null) Some(sbi_clerk_mains(1)) else None
      val sbi_clerk_prelims_2nd = if(sbi_clerk_prelims != null) Some(sbi_clerk_prelims(1)) else None
      val assam_cee_2nd = if(assam_cee != null) Some(assam_cee(1)) else None

      val observedPcOption = List(bitsat_2nd, gujarat_cet_2nd, jee_main_2nd, ibps_clerk_mains_2nd, mhcet_2nd, foundation_09_2nd, foundation_08_2nd, mht_cet_2nd, sbi_po_prelims_2nd, ts_eamcet_2nd, aiims_2nd, mh_cet_2nd, sbi_po_mains_2nd, ap_eamcet_2nd, foundation_10_2nd, jee_advanced_2nd, jipmer_2nd, neet_2nd, jee_hindi_2nd, sbi_clerk_mains_2nd, sbi_clerk_prelims_2nd, assam_cee_2nd)

      val observedPcList = observedPcOption.flatten.distinct

      val pc = if(observedPcList.contains("A")) {
        "A"
      } else if(observedPcList.contains("P")) {
        "P"
      } else if(observedPcList.contains("F")) {
        "F"
      } else if(observedPcList.contains("NA")) {
        "NA"
      } else {
        "NA"
      }

      pc
    }

    val pcUDF: UserDefinedFunction = df.sparkSession.udf.register("pcAnonFunc", pcAnonFunc)

    import df.sparkSession.implicits._

    val recordTypeFilterList = List("test", "practice", "clickstream", "server-logs-session", "monetization", "user_summary", "learn_session", "learn_search", "learn_browse")
    val filteredDF = df
      .filter(col("record_type").isin(recordTypeFilterList: _*))

    val inputFlatDF = filteredDF
      .withColumn("is_rankup_user", col("user_fsm_state.is_rankup_user"))
      .withColumn("is_jump_user", col("user_fsm_state.is_jump_user"))
      .withColumn("is_paid_user", col("user_fsm_state.is_paid_user"))
      .withColumn("highest_uc", col("user_fsm_state.highest_uc"))
      .withColumn("current_uc", col("user_fsm_state.current_uc"))
      .withColumn("fsm_updated_at", col("user_fsm_state.fsm_updated_at"))
      .withColumn("primary_goal_id", col("user_fsm_state.primary_goal_id"))
      .withColumn("primary_organization_id", col("user_fsm_state.primary_organization_id"))
      .withColumn("total_clickstream_sessions", col("user_fsm_state.total_clickstream_sessions"))
      .withColumn("total_learn_browses", col("user_fsm_state.total_learn_browses"))
      .withColumn("total_learn_searches", col("user_fsm_state.total_learn_searches"))
      .withColumn("total_learn_sessions", col("user_fsm_state.total_learn_sessions"))
      .withColumn("total_looked", col("user_fsm_state.total_looked"))
      .withColumn("total_practice_attempts", col("user_fsm_state.total_practice_attempts"))
      .withColumn("total_practice_corrects", col("user_fsm_state.total_practice_corrects"))
      .withColumn("total_practice_sessions", col("user_fsm_state.total_practice_sessions"))
      .withColumn("total_server_sessions", col("user_fsm_state.total_server_sessions"))
      .withColumn("total_test_attempts", col("user_fsm_state.total_test_attempts"))
      .withColumn("total_test_corrects", col("user_fsm_state.total_test_corrects"))
      .withColumn("total_test_sessions", col("user_fsm_state.total_test_sessions"))
      .withColumn("total_valid_chapter_tests", col("user_fsm_state.total_valid_chapter_tests"))
      .withColumn("total_valid_full_tests", col("user_fsm_state.total_valid_full_tests"))
      .withColumn("total_valid_other_tests", col("user_fsm_state.total_valid_other_tests"))
      .withColumn("total_valid_tests", col("user_fsm_state.total_valid_tests"))
      .withColumn("learn_timespent", col("user_fsm_state.all_time_per_property_timespent.learn_timespent"))
      .withColumn("overall_timespent", col("user_fsm_state.all_time_per_property_timespent.overall_timespent"))
      .withColumn("practice_timespent", col("user_fsm_state.all_time_per_property_timespent.practice_timespent"))
      .withColumn("test_timespent", col("user_fsm_state.all_time_per_property_timespent.test_timespent"))
      .withColumn("pc", pcUDF(
        col("user_fsm_state.pc.bitsat"),
        col("user_fsm_state.pc.gujarat-cet"),
        col("user_fsm_state.pc.jee-main"),
        col("user_fsm_state.pc.ibps-clerk-mains"),
        col("user_fsm_state.pc.mhcet"),
        col("user_fsm_state.pc.foundation-09"),
        col("user_fsm_state.pc.foundation-08"),
        col("user_fsm_state.pc.mht-cet"),
        col("user_fsm_state.pc.sbi-po-prelims"),
        col("user_fsm_state.pc.ts-eamcet"),
        col("user_fsm_state.pc.aiims"),
        col("user_fsm_state.pc.mh-cet"),
        col("user_fsm_state.pc.sbi-po-mains"),
        col("user_fsm_state.pc.ap-eamcet"),
        col("user_fsm_state.pc.foundation-10"),
        col("user_fsm_state.pc.jee-advanced"),
        col("user_fsm_state.pc.jipmer"),
        col("user_fsm_state.pc.neet"),
        col("user_fsm_state.pc.jee-hindi"),
        col("user_fsm_state.pc.sbi-clerk-mains"),
        col("user_fsm_state.pc.sbi-clerk-prelims"),
        col("user_fsm_state.pc.assam-cee")))

    val dateDF = inputFlatDF
      .withColumn("date", to_date(col("started_at")))
      .withColumn("year", year(col("date")))
      .withColumn("month", month(col("date")))
      .withColumn("week", weekofyear(col("date")))
      .withColumn("month_day", dayofmonth(col("date")))
      .drop("user_fsm_state")

    val inputDS = dateDF.as[GovInputSchema]

    inputDS
  }

  val govInputSchema: StructType = {
    val pc = StructType(
      Seq(
        StructField("bitsat", ArrayType(StringType)),
        StructField("gujarat-cet", ArrayType(StringType)),
        StructField("jee-main", ArrayType(StringType)),
        StructField("ibps-clerk-mains", ArrayType(StringType)),
        StructField("mhcet", ArrayType(StringType)),
        StructField("foundation-09", ArrayType(StringType)),
        StructField("foundation-08", ArrayType(StringType)),
        StructField("mht-cet", ArrayType(StringType)),
        StructField("sbi-po-prelims", ArrayType(StringType)),
        StructField("ts-eamcet", ArrayType(StringType)),
        StructField("aiims", ArrayType(StringType)),
        StructField("mh-cet", ArrayType(StringType)),
        StructField("sbi-po-mains", ArrayType(StringType)),
        StructField("ap-eamcet", ArrayType(StringType)),
        StructField("foundation-10", ArrayType(StringType)),
        StructField("jee-advanced", ArrayType(StringType)),
        StructField("jipmer", ArrayType(StringType)),
        StructField("neet", ArrayType(StringType)),
        StructField("jee-hindi", ArrayType(StringType)),
        StructField("sbi-clerk-mains", ArrayType(StringType)),
        StructField("sbi-clerk-prelims", ArrayType(StringType)),
        StructField("assam-cee", ArrayType(StringType))
      )
    )
    val all_time_per_property_timespent = StructType(
      Seq(
        StructField("learn_timespent", StringType),
        StructField("overall_timespent", StringType),
        StructField("practice_timespent", StringType),
        StructField("test_timespent", StringType)
      )
    )
    val user_fsm_state = StructType(
      Seq(
        StructField("is_rankup_user", StringType),
        StructField("is_jump_user", StringType),
        StructField("is_paid_user", StringType), //
        StructField("highest_uc", StringType),
        StructField("current_uc", StringType),
        StructField("fsm_updated_at", StringType),
        StructField("primary_goal_id", StringType),
        StructField("primary_organization_id", StringType),
        StructField("total_clickstream_sessions", StringType),
        StructField("total_learn_browses", StringType),
        StructField("total_learn_searches", StringType),
        StructField("total_learn_sessions", StringType),
        StructField("total_looked", StringType),
        StructField("total_practice_attempts", StringType),
        StructField("total_practice_corrects", StringType),
        StructField("total_practice_sessions", StringType),
        StructField("total_server_sessions", StringType),
        StructField("total_test_attempts", StringType),
        StructField("total_test_corrects", StringType),
        StructField("total_test_sessions", StringType),
        StructField("total_valid_chapter_tests", StringType),
        StructField("total_valid_full_tests", StringType),
        StructField("total_valid_other_tests", StringType),
        StructField("total_valid_tests", StringType),
        StructField("all_time_per_property_timespent", all_time_per_property_timespent),
        StructField("pc", pc)
      )
    )
    val rootSchema = StructType(
      Seq(
        StructField("user_id", StringType),
        StructField("started_at", StringType),
        StructField("record_type", StringType),
        StructField("user_fsm_state", user_fsm_state)
      )
    )
    rootSchema
  }
}

case class GovInputSchema(
                           user_id: String,
                           record_type: String,
                           is_rankup_user: String,
                           is_jump_user: String,
                           is_paid_user: String,
                           highest_uc: String,
                           current_uc: String,
                           fsm_updated_at: String,
                           primary_goal_id: String,
                           primary_organization_id: String,
                           total_clickstream_sessions: String,
                           total_learn_browses: String,
                           total_learn_searches: String,
                           total_learn_sessions: String,
                           total_looked: String,
                           total_practice_attempts: String,
                           total_practice_corrects: String,
                           total_practice_sessions: String,
                           total_server_sessions: String,
                           total_test_attempts: String,
                           total_test_corrects: String,
                           total_test_sessions: String,
                           total_valid_chapter_tests: String,
                           total_valid_full_tests: String,
                           total_valid_other_tests: String,
                           total_valid_tests: String,
                           learn_timespent: String,
                           overall_timespent: String,
                           practice_timespent: String,
                           test_timespent: String,
                           pc: String,
                           started_at: String,
                           date: Date,
                           year: Int,
                           month: Int,
                           week: Int,
                           month_day: Int
                         )
