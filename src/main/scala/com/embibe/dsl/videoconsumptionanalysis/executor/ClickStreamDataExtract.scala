package com.embibe.dsl.videoconsumptionanalysis.executor

import java.sql.Date

import com.embibe.dsl.videoconsumptionanalysis.aws.s3.ObjectLister.getS3Objects
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Dataset, SaveMode, SparkSession}
import org.apache.spark.storage.StorageLevel
import org.joda.time.DateTime

/** Clickstream data extractor
  *
  * spark-shell --master yarn --driver-memory 2G --executor-memory 4G --num-executors 8 --executor-cores 4 --packages org.elasticsearch:elasticsearch-hadoop:6.3.1,com.typesafe:config:1.3.2,com.amazonaws:aws-java-sdk-s3:1.11.438 --queue dev_cs_agg --name dev_shell
  */
object ClickStreamDataExtract {
  def main(args: Array[String]): Unit = {
    // SparkSession
    val spark = SparkSession.builder
      .master("yarn-client")
      .appName(getClass.getSimpleName)
      .enableHiveSupport()
      .getOrCreate()
    import spark.implicits._

    // ---- Extract ----
    // COV
    val s3BucketName = "dsl-datastore"
    val covDirPrefix = "data/content/cov/prod/exam_wise_cov_data/20181203/"
    val covPathList = getS3Objects(s3BucketName, covDirPrefix).filter(_.contains(".json")).map(s"s3a://$s3BucketName/" + _)
    val covDF = spark.read.schema(covSchema).json(covPathList: _*)
    covDF.persist(StorageLevel.MEMORY_AND_DISK)

    // Broadcast Variables
    val goalCOVDF = getUnitIdCodeMap(covDF, "goal_id", "goal_code", "goal_name")
    val examCOVDF = getUnitIdCodeMap(covDF, "exam_id", "exam_code", "exam_name")
    val subjectCOVDF = getUnitIdCodeMap(covDF, "subject_id", "subject_code", "subject_name")
    val unitCOVDF = getUnitIdCodeMap(covDF, "unit_id", "unit_code", "unit_name")
    val chapterCOVDF = getUnitIdCodeMap(covDF, "chapter_id", "chapter_code", "chapter_name")
    val conceptCOVDF = getUnitIdCodeMap(covDF, "concept_id", "concept_code", "concept_name")
    val covDS = covDF
      .select("content_code", "goal_code", "goal_name", "exam_code", "exam_name", "subject_code", "subject_name", "unit_code", "unit_name", "chapter_code", "chapter_name", "concept_code", "concept_name")
      .as[CovInputSchema]
    val contentCovList = getContentCovList(covDS)

    // Config
    val config = getConfig
    val analysisConfig = config.getConfig("analysis")
    val analysisRangeConfig = analysisConfig.getConfig("range")
    val startDate = new DateTime(Date.valueOf(analysisRangeConfig.getString("start")).getTime)
    val endDate = new DateTime(Date.valueOf(analysisRangeConfig.getString("end")).getTime)

    // Input Clickstream Data
    val jobStartTimestamp = spark.sparkContext.startTime
    val nonRearchDirPrefix = "data/ingested/clickstream/raw/rearch/"
    val nonRearchInputPathList = getS3InputPathList(s3BucketName, nonRearchDirPrefix, startDate, endDate)
    val inputPathList = nonRearchInputPathList.map(s"s3a://$s3BucketName/" + _)
    // Reading Input files using spark
    val inputDF = spark.read.schema(inputSchema).json(inputPathList: _*)
    val inputDS = getInputDS(inputDF)

    // ---- Transform ----
    // mapping url with learning map
    val clickStreamLearningMapDF = getUrlMappedDS(inputDS, contentCovList, goalCOVDF, examCOVDF, subjectCOVDF, unitCOVDF, chapterCOVDF, conceptCOVDF)

    // ---- Intermediate Load ----
    val intermediateClickStreamPath = "s3a://dsl-datastore/data/video-analysis/clickstream-learningmap/year/2018/"
    clickStreamLearningMapDF.write.mode(SaveMode.Overwrite).parquet(intermediateClickStreamPath)
  }

  def getUnitIdCodeMap(df: DataFrame, idColumn: String, codeColumn: String, nameColumn: String) = {
    val distinctDF = df.select(idColumn, codeColumn, nameColumn).distinct()
      .filter(col(codeColumn).isNotNull && col(codeColumn) =!= "")
      .withColumnRenamed(codeColumn, "codeColumn")
      .withColumnRenamed(idColumn, "idColumn")
      .withColumnRenamed(nameColumn, "nameColumn")

    val distinctMapArray = distinctDF
      .collect()
      .map(r => Map(distinctDF.columns.zip(r.toSeq.map(_.toString)): _*))
    val distinctMap = Map(distinctMapArray.map(p => (p("idColumn").trim.toLowerCase, p)):_*)

    df.sparkSession.sparkContext.broadcast(distinctMap)
  }

  def getUrlMappedDS(ds: Dataset[InputSchema],
                     contentCovList: Broadcast[scala.List[CovInputSchema]],
                     goalCOVDF: Broadcast[Map[String, Map[String, String]]],
                     examCOVDF: Broadcast[Map[String, Map[String, String]]],
                     subjectCOVDF: Broadcast[Map[String, Map[String, String]]],
                     unitCOVDF: Broadcast[Map[String, Map[String, String]]],
                     chapterCOVDF: Broadcast[Map[String, Map[String, String]]],
                     conceptCOVDF:Broadcast[Map[String, Map[String, String]]]
                    ) = {
    import ds.sparkSession.implicits._
    val urlMappedDS = ds.flatMap{ is =>
      val splitList = is.url.split("entity_code=").toList

      val entitiyCodeOption = if(splitList.length < 2) None else Some(splitList.last)
      val urlMappedInputRowList = mapAllContentAttributes(is, entitiyCodeOption, contentCovList)

      // Over-writing primary entity
        val contentType = is.content_type
        val contentId = is.content_id

      if (contentType != null) {
        contentType match {
          case x if x == "goal" => {
            val codeOption = getDistinctTokenCode(contentId, goalCOVDF)
            if (codeOption.isDefined) urlMappedInputRowList.map(x => x.copy(goal_code = codeOption.get._1, goal_name = codeOption.get._2))
            else urlMappedInputRowList
          }
          case x if x == "exam" => {
            val codeOption = getDistinctTokenCode(contentId, examCOVDF)
            if (codeOption.isDefined) urlMappedInputRowList.map(x => x.copy(exam_code = codeOption.get._1, exam_name = codeOption.get._2))
            else urlMappedInputRowList
          }
          case x if x == "subject" => {
            val codeOption = getDistinctTokenCode(contentId, subjectCOVDF)
            if (codeOption.isDefined) urlMappedInputRowList.map(x => x.copy(subject_code = codeOption.get._1, subject_name = codeOption.get._2))
            else urlMappedInputRowList
          }
          case x if x == "unit" => {
            val codeOption = getDistinctTokenCode(contentId, unitCOVDF)
            if (codeOption.isDefined) urlMappedInputRowList.map(x => x.copy(unit_code = codeOption.get._1, unit_name = codeOption.get._2))
            else urlMappedInputRowList
          }
          case x if x == "chapter" => {
            val codeOption = getDistinctTokenCode(contentId, chapterCOVDF)
            if (codeOption.isDefined) urlMappedInputRowList.map(x => x.copy(chapter_code = codeOption.get._1, chapter_name = codeOption.get._2))
            else urlMappedInputRowList
          }
          case x if x == "concept" => {
            val codeOption = getDistinctTokenCode(contentId, conceptCOVDF)
            if (codeOption.isDefined) urlMappedInputRowList.map(x => x.copy(concept_code = codeOption.get._1, concept_name = codeOption.get._2))
            else urlMappedInputRowList
          }
          case _ => urlMappedInputRowList
        }
      } else {
        urlMappedInputRowList
      }
    }

    urlMappedDS
  }

  def getDistinctTokenCode(tokenId: String,
                           tokenNameCodeMapBroadcast: Broadcast[Map[String, Map[String, String]]]) = {
    val tokenNameCodeMapValue = tokenNameCodeMapBroadcast.value
    val mapOption = tokenNameCodeMapValue.get(tokenId)
    if (mapOption.isDefined) {
      val codeMap = mapOption.get
      Some((codeMap("codeColumn"), codeMap("nameColumn")))
    } else {
      None
    }
  }

  def mapAllContentAttributes(
                               row: InputSchema,
                               entityCodeOption: Option[String],
                               contentCovList: Broadcast[scala.List[CovInputSchema]]
                             ): List[UrlMappedSchema] = {
    val contentCovListValue = contentCovList.value
    // Filtering COV
    val filterCovData = if(entityCodeOption.isDefined) {
      val entityCode = entityCodeOption.get.trim.toLowerCase()
      contentCovListValue.filter { cov =>
        if(cov.content_code != null && cov.content_code.nonEmpty) {
          cov.content_code.trim.toLowerCase == entityCode
        } else {
          false
        }
      }
    } else {
      List.empty[CovInputSchema]
    }

    if(filterCovData.isEmpty) {
      List(
        UrlMappedSchema(
          user_id = row.user_id,
          url = row.url,
          user_agent = row.user_agent,
          session_id = row.session_id,
          event_code = row.event_code,
          event_name = row.event_name,
          event_type = row.event_type,
          video_state = row.video_state,
          video_url = row.video_url,
          video_time_position = row.video_time_position,
          content_id = row.content_id,
          content_type = row.content_type,
          timestamp = row.timestamp,
          date = row.date,
          year = row.year,
          month = row.month,
          week = row.week,
          month_day = row.month_day,
          hour = row.hour,
          goal_code = "",
          goal_name = "",
          exam_code = "",
          exam_name = "",
          subject_code = "",
          subject_name = "",
          unit_code = "",
          unit_name = "",
          chapter_code = "",
          chapter_name = "",
          concept_code = "",
          concept_name = ""
        )
      )
    }
    else {
      filterCovData.map { cov =>
        UrlMappedSchema(
          user_id = row.user_id,
          url = row.url,
          user_agent = row.user_agent,
          session_id = row.session_id,
          event_code = row.event_code,
          event_name = row.event_name,
          event_type = row.event_type,
          video_state = row.video_state,
          video_url = row.video_url,
          video_time_position = row.video_time_position,
          content_id = row.content_id,
          content_type = row.content_type,
          timestamp = row.timestamp,
          date = row.date,
          year = row.year,
          month = row.month,
          week = row.week,
          month_day = row.month_day,
          hour = row.hour,
          goal_code = cov.goal_code,
          goal_name = cov.goal_name,
          exam_code = cov.exam_code,
          exam_name = cov.exam_name,
          subject_code = cov.subject_code,
          subject_name = cov.subject_name,
          unit_code = cov.unit_code,
          unit_name = cov.unit_name,
          chapter_code = cov.chapter_code,
          chapter_name = cov.chapter_name,
          concept_code = cov.concept_code,
          concept_name = cov.concept_name
        )
      }
    }
  }

  // TODO: Wrong! - Fix Year Bug for Different Years
  def getS3InputPathList(bucketName: String, dirPrefix: String, startDate: DateTime, endDate: DateTime): List[String] = {
    val (startYear, startMonth)= (startDate.getYear, startDate.getMonthOfYear)
    val (endYear, endMonth)= (endDate.getYear, endDate.getMonthOfYear)

    val inputPathList = if (startDate.getMillis <= endDate.getMillis) {

      val pathListOption = (startYear to endYear by 1).toList.flatMap{ year =>
        val newEndMonth = if (year == endYear) endMonth else 12
        (startMonth to newEndMonth by 1).toList.map{ month =>
          val updatedDirPrefix = dirPrefix + year + "/" + {if (month < 10) "0" + month else month.toString}
          getS3Objects(bucketName, updatedDirPrefix)
        }
      }.reduceOption(_ ++ _)
      if (pathListOption.isDefined) pathListOption.get else List.empty[String]

    } else {
      throw new IllegalArgumentException(getClass.getSimpleName + "- Range Exception: startDate should be less than endDate")
    }

    inputPathList
  }

  def getInputDS(df: DataFrame): Dataset[InputSchema] = {
    import df.sparkSession.implicits._

    val pauseEdgeCaseFilterUDF = udf({(videoState: String, timePosition: Double) =>
      if(videoState == null || timePosition == null) true
      else if(videoState.trim.toLowerCase() == "pause") timePosition != 0
      else true
    })

    val inputFlatDF = df
      .withColumn("user_id", trim(coalesce(col("userId"), col("properties.user_id"))))
      .withColumn("url", col("context.page.url"))
      .withColumn("user_agent", col("context.userAgent"))
      .withColumn("session_id", col("properties.session_id"))
      .withColumn("event_code", col("properties.event_code"))
      .withColumn("event_name", col("properties.event_name"))
      .withColumn("event_type", col("properties.event_type"))
      .withColumn("video_state", col("properties.extra_params.state"))
      .withColumn("video_url", col("properties.extra_params.url"))
      .withColumn("video_time_position", col("properties.extra_params.time_position"))
      .withColumn("content_id", col("properties.extra_params.content_id"))
      .withColumn("content_type", col("properties.extra_params.content_type"))
      .drop("context")
      .drop("properties")
      .drop("userId")
      .filter(col("context.page.url").contains("/study/"))

    val dateDF = inputFlatDF
      .withColumn("video_time_position", when(col("video_time_position").isNull, 0).otherwise(col("video_time_position")))
      .withColumn("date", to_date(col("timestamp")))
      .withColumn("year", year(col("date")))
      .withColumn("month", month(col("date")))
      .withColumn("week", weekofyear(col("date")))
      .withColumn("month_day", dayofmonth(col("date")))
      .withColumn("hour", hour(to_timestamp(col("timestamp"))))

    val filterDF = dateDF
      .filter(trim(col("event_name")) === lit("play/pause video") || trim(col("event_name")) === lit("page load"))
      .filter(pauseEdgeCaseFilterUDF(col("video_state"), col("video_time_position")))

    val inputDS = filterDF.as[InputSchema]

    inputDS
  }

  def getContentCovList(df: Dataset[CovInputSchema]): Broadcast[scala.List[CovInputSchema]] = {
    val distinctMapList = df
      .collect()
      .toList

    df.sparkSession.sparkContext.broadcast(distinctMapList)
  }

  def getConfig: Config = {
    val urlParserString =
      """
        |{
        |  "analysis": {
        |    "range": {
        |      "start": "2018-01-01",
        |      "end": "2018-11-01"
        |    }
        |  }
        |}
      """.stripMargin

    ConfigFactory.parseString(urlParserString)
  }

  val covSchema: StructType = {
    val rootSchema = StructType(
      Seq(
        StructField("content_code", StringType),
        StructField("goal_code", StringType),
        StructField("goal_name", StringType),
        StructField("goal_id", StringType),
        StructField("exam_code", StringType),
        StructField("exam_name", StringType),
        StructField("exam_id", StringType),
        StructField("subject_code", StringType),
        StructField("subject_name", StringType),
        StructField("subject_id", StringType),
        StructField("unit_code", StringType),
        StructField("unit_name", StringType),
        StructField("unit_id", StringType),
        StructField("chapter_code", StringType),
        StructField("chapter_name", StringType),
        StructField("chapter_id", StringType),
        StructField("concept_code", StringType),
        StructField("concept_name", StringType),
        StructField("concept_id", StringType)
      )
    )
    rootSchema
  }

  val inputSchema: StructType = {
    val propertiesExtraParamsSchema = StructType(
      Seq(
        StructField("state", StringType),
        StructField("url", StringType),
        StructField("time_position", DoubleType),
        StructField("content_id", StringType),
        StructField("content_type", StringType)
      )
    )
    val propertiesSchema = StructType(
      Seq(
        StructField("user_id", StringType),
        StructField("session_id", StringType),
        StructField("event_code", StringType),
        StructField("event_name", StringType),
        StructField("event_type", StringType),
        StructField("extra_params", propertiesExtraParamsSchema)
      )
    )
    val pageSchema = StructType(
      Seq(
        StructField("url", StringType)
      )
    )
    val contextSchema = StructType(
      Seq(
        StructField("userAgent", StringType),
        StructField("page", pageSchema)
      )
    )
    val rootSchema = StructType(
      Seq(
        StructField("userId", StringType),
        StructField("timestamp", StringType),
        StructField("properties", propertiesSchema),
        StructField("context", contextSchema)
      )
    )
    rootSchema
  }
}

case class InputSchema(
                        user_id: String,
                        url: String,
                        user_agent: String,
                        session_id: String,
                        event_code: String,
                        event_name: String,
                        event_type: String,
                        video_state: String,
                        video_url: String,
                        video_time_position: Double,
                        content_id: String,
                        content_type: String,
                        timestamp: String,
                        date: Date,
                        year: Int,
                        month: Int,
                        week: Int,
                        month_day: Int,
                        hour: Int
                      )

case class CovInputSchema(
                           content_code: String,
                           goal_code: String,
                           goal_name: String,
                           exam_code: String,
                           exam_name: String,
                           subject_code: String,
                           subject_name: String,
                           unit_code: String,
                           unit_name: String,
                           chapter_code: String,
                           chapter_name: String,
                           concept_code: String,
                           concept_name: String
                         )

case class UrlMappedSchema(
                            user_id: String,
                            url: String,
                            user_agent: String,
                            session_id: String,
                            event_code: String,
                            event_name: String,
                            event_type: String,
                            video_state: String,
                            video_url: String,
                            video_time_position: Double,
                            content_id: String,
                            content_type: String,
                            timestamp: String,
                            date: Date,
                            year: Int,
                            month: Int,
                            week: Int,
                            month_day: Int,
                            hour: Int,
                            goal_code: String,
                            goal_name: String,
                            exam_code: String,
                            exam_name: String,
                            subject_code: String,
                            subject_name: String,
                            unit_code: String,
                            unit_name: String,
                            chapter_code: String,
                            chapter_name: String,
                            concept_code: String,
                            concept_name: String
                          )

case class UserIdAggregateSchema(
                                  code: String,
                                  name: String,
                                  user_id_list: List[String]
                                )

case class AggregateCountSchema(
                                 codeColumn: String,
                                 nameColumn: String,
                                 countColumn: Long
                                )
