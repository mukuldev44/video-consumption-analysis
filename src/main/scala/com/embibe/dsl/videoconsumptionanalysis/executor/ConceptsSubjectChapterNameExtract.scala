package com.embibe.dsl.videoconsumptionanalysis.executor

import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.functions._

object ConceptsSubjectChapterNameExtract {
  def main(args: Array[String]): Unit = {
    // SparkSession
    val spark = SparkSession.builder
      .master("yarn-client")
      .appName(getClass.getSimpleName)
      .enableHiveSupport()
      .getOrCreate()
    import spark.implicits._

    val gsContentDF = getElasticDF(spark,
      "embibe-gs-content-ms-concepts-init",
      "gs.content.concepts",
      "10.140.10.24")
    val conceptDistinctDF = gsContentDF
      .select("concept_code", "subject_name", "chapter_name")
      .filter(col("concept_code").isNotNull)
      .dropDuplicates("concept_code")
      .cache()
    val analysisOutputPrefixPath = "s3a://dsl-datastore/data/video-analysis/output"
    val videoPlayDF = spark.read.option("header", "true").csv(analysisOutputPrefixPath + "/count-aggregation/year/2018")

    val videoPlaySubjectChapterNameDF = videoPlayDF
      .join(conceptDistinctDF, videoPlayDF("code") === conceptDistinctDF("concept_code"), "left_outer")
      .drop(conceptDistinctDF("concept_code"))

    videoPlaySubjectChapterNameDF
      .coalesce(1)
      .write
      .option("header", "true")
      .mode(SaveMode.Overwrite)
      .csv(analysisOutputPrefixPath + "/count-aggregation/concept-subject-chapter-name/year/2018")
  }
  /** Reading Data From Elasticsearch
    *
    * Issue: org.elasticsearch.hadoop.rest.EsHadoopInvalidRequest: too_many_clauses: maxClauseCount is set to 1024
    * Solution: pushdown -> false
    *
    * @param spark
    * @param indexName
    * @param docType
    * @param ip
    * @return
    */
  def getElasticDF(spark: SparkSession,
                   indexName: String,
                   docType: String,
                   ip: String): DataFrame = {
    spark.read.format("org.elasticsearch.spark.sql")
      .option("es.nodes", ip)
      .option("es.port", "9200")
      .option("pushdown", "true")
      .option("es.scroll.size", "10000")
      .load(indexName+"/"+docType)
  }
}
