package com.embibe.dsl.videoconsumptionanalysis.executor

import eu.bitwalker.useragentutils.UserAgent
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.functions._

/**
  * spark-shell --master yarn --driver-memory 2G --executor-memory 2G --num-executors 5 --executor-cores 4 --packages org.elasticsearch:elasticsearch-hadoop:6.3.1,com.typesafe:
  * nfig:1.3.2,com.amazonaws:aws-java-sdk-s3:1.11.438,eu.bitwalker:UserAgentUtils:1.21 --name dev_shell2
  */
object DeviceAnalytics {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder
      .master("yarn-client")
      .appName(getClass.getSimpleName)
      .enableHiveSupport()
      .getOrCreate()
    import spark.implicits._

    // ---- Extract ----
    val clickstreamDS = spark.read.parquet("s3a://dsl-datastore/data/video-analysis/clickstream-learningmap/year/2018")
      .as[UrlMappedSchema]
//      .filter(_.month == 4)

    val filterDF = clickstreamDS
      .filter(trim(col("event_name")) === lit("page load") ||
        (trim(col("event_name")) === lit("play/pause video") && trim(col("video_state")) =!= "pause"))

    val deviceTypeDF = filterDF.withColumn("device_type", deviceTypeUDF(col("user_agent")))
    val analysisOutputPrefixPath = "s3a://dsl-datastore/data/video-analysis/output"
    val videoPlayTagDF = getVideoLikelyhoodTagDF(deviceTypeDF)
    val deviceVideoLikelyhoodDF = getDeviceTypeVideolikelyhood(videoPlayTagDF)
    deviceVideoLikelyhoodDF
      .coalesce(1)
      .write.mode(SaveMode.Overwrite)
      .option("header", "true")
      .csv(analysisOutputPrefixPath + "/device-videolikelyhood/year/2018")
  }

  val deviceTypeUDF = udf{(userAgent: String) =>
    val deviceType = UserAgent.parseUserAgentString(userAgent).getOperatingSystem.getDeviceType
    deviceType.toString
  }

  def getVideoLikelyhoodTagDF(joinDF: DataFrame) = {
    val pageLoadFilterUDF = udf((eventNameSeq: Seq[String]) => if(eventNameSeq.contains("page load")) true else false)
    val videoTagUDF = udf((eventNameSeq: Seq[String]) => if(eventNameSeq.contains("play/pause video")) 1 else 0)

    val w1 = Window
      .partitionBy(
        col("user_id"),
        col("session_id"),
        col("url"),
        col("year"),
        col("month"),
        col("month_day"),
        col("hour"),
        window(to_timestamp(col("timestamp")), "5 minutes"))

    val eventNameSetDF = joinDF.withColumn("event_name_list", collect_set(lower(trim(col("event_name")))).over(w1))
    val pageLoadFilterDF = eventNameSetDF.filter(pageLoadFilterUDF(col("event_name_list")))
    val videoPlayFlagDF = pageLoadFilterDF
      .withColumn("video_play_flag", videoTagUDF(col("event_name_list"))).drop("event_name_list")

    val w2 = Window
      .partitionBy(
        col("user_id"),
        col("session_id"),
        col("url"),
        col("year"),
        col("month"),
        col("month_day"),
        col("hour"),
        window(to_timestamp(col("timestamp")), "5 minutes"),
        col("device_type"))
      .orderBy("timestamp")

    videoPlayFlagDF.withColumn("rn", row_number().over(w2)).filter(col("rn") === 1).drop("rn")
  }

  def getDeviceTypeVideolikelyhood(videoSessionPlayTagDF: DataFrame) = {
    val percentageUDF = udf{(total: Long, videoPlay: Long) =>
      if(total != 0) {
        (videoPlay.toDouble * 100) / total
      } else {
        0.0
      }
    }
    val s = videoSessionPlayTagDF
      .groupBy("device_type")
      .agg(count("video_play_flag").as("total"), sum("video_play_flag").as("video_play"))
      .na.fill(0)

    s.withColumn("percentage", percentageUDF(col("total"), col("video_play")))
 }
}
