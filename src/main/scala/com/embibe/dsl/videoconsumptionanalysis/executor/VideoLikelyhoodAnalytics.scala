package com.embibe.dsl.videoconsumptionanalysis.executor

import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions.Window

object VideoLikelyhoodAnalytics {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder
      .master("yarn-client")
      .appName(getClass.getSimpleName)
      .enableHiveSupport()
      .getOrCreate()
    import spark.implicits._

    val clickstreamDS = spark.read.parquet("s3a://dsl-datastore/data/video-analysis/clickstream-learningmap/year/2018")
      .as[UrlMappedSchema]
//      .filter(_.month == 4)

    val userGovDF = spark.read.parquet("s3a://dsl-datastore/data/video-analysis/user-gov/year/2018/")
      .as[GovInputSchema]
//      .filter(_.month == 4)
      .withColumn("pc", when(col("pc").isNull, lit("NA")).otherwise(col("pc")))

    val joinDF = userGovDF.join(clickstreamDS,
      userGovDF("user_id") === clickstreamDS("user_id") && userGovDF("month_day") === clickstreamDS("month_day"))
      .drop(userGovDF("user_id"))
      .drop(userGovDF("started_at"))
      .drop(userGovDF("date"))
      .drop(userGovDF("year"))
      .drop(userGovDF("month"))
      .drop(userGovDF("week"))
      .drop(userGovDF("month_day"))

    val joinDF1 = joinDF
      .filter(trim(col("event_name")) === lit("page load") ||
        (trim(col("event_name")) === lit("play/pause video") && trim(col("video_state")) =!= "pause"))

    val analysisOutputPrefixPath = "s3a://dsl-datastore/data/video-analysis/output"
    val videoSessionPlayTagDF =  getVideoLikelyhoodTagDF(joinDF1)
    val pcVideoLikelyhoodDF = getPCVideolikelyhood(videoSessionPlayTagDF)
    pcVideoLikelyhoodDF
      .coalesce(1)
      .write.mode(SaveMode.Overwrite)
      .option("header", "true")
      .csv(analysisOutputPrefixPath + "/pc-videolikelyhood/year/2018")
    val ucVideoLikelyhoodDF = getUCVideolikelyhood(videoSessionPlayTagDF)
    ucVideoLikelyhoodDF
      .coalesce(1)
      .write.mode(SaveMode.Overwrite)
      .option("header", "true")
      .csv(analysisOutputPrefixPath + "/uc-videolikelyhood/year/2018")
  }

  def getUCVideolikelyhood(videoSessionPlayTagDF: DataFrame) = {
    val percentageUDF = udf{(total: Long, videoPlay: Long) =>
      if(total != 0) {
        (videoPlay.toDouble * 100) / total
      } else {
        0.0
      }
    }
    val s = videoSessionPlayTagDF
      .groupBy("current_uc")
      .agg(count("video_play_flag").as("total"), sum("video_play_flag").as("video_play"))
      .na.fill(0)

    s.withColumn("percentage", percentageUDF(col("total"), col("video_play")))
  }

  def getPCVideolikelyhood(videoSessionPlayTagDF: DataFrame) = {
    val percentageUDF = udf{(total: Long, videoPlay: Long) =>
      if(total != 0) {
        (videoPlay.toDouble * 100) / total
      } else {
        0.0
      }
    }
    val s = videoSessionPlayTagDF
      .groupBy("pc")
      .agg(count("video_play_flag").as("total"), sum("video_play_flag").as("video_play"))
      .na.fill(0)

    s.withColumn("percentage", percentageUDF(col("total"), col("video_play")))
  }

  def getVideoLikelyhoodTagDF(joinDF: DataFrame) = {
    val pageLoadFilterUDF = udf((eventNameSeq: Seq[String]) => if(eventNameSeq.contains("page load")) true else false)
    val videoTagUDF = udf((eventNameSeq: Seq[String]) => if(eventNameSeq.contains("play/pause video")) 1 else 0)

    val w1 = Window
      .partitionBy(
        col("user_id"),
        col("session_id"),
        col("url"),
        col("year"),
        col("month"),
        col("month_day"),
        col("hour"),
        window(to_timestamp(col("timestamp")), "5 minutes"))

    val eventNameSetDF = joinDF.withColumn("event_name_list", collect_set(lower(trim(col("event_name")))).over(w1))
    val pageLoadFilterDF = eventNameSetDF.filter(pageLoadFilterUDF(col("event_name_list")))
    val videoPlayFlagDF = pageLoadFilterDF
      .withColumn("video_play_flag", videoTagUDF(col("event_name_list"))).drop("event_name_list")

    val w2 = Window
      .partitionBy(
        col("user_id"),
        col("session_id"),
        col("url"),
        col("year"),
        col("month"),
        col("month_day"),
        col("hour"),
        window(to_timestamp(col("timestamp")), "5 minutes"),
        col("pc"),
        col("current_uc"))
      .orderBy("timestamp")

    videoPlayFlagDF.withColumn("rn", row_number().over(w2)).filter(col("rn") === 1).drop("rn")
  }

//  def getPCLearningmapVideolikelyhood(videoSessionPlayTagDF: DataFrame) = {
//    val percentageUDF = udf{(total: Long, videoPlay: Long) =>
//      if(total != 0) {
//        (videoPlay.toDouble * 100) / total
//      } else {
//        0.0
//      }
//    }
//    val s = videoSessionPlayTagDF
//      .groupBy( "concept_code", "concept_name")
//      .pivot("pc")
//      .agg(count("video_play_flag").as("total"), sum("video_play_flag").as("video_play"))
//      .na.fill(0)
//
//    val perDF = s
//      .withColumn("A_percentage", percentageUDF(col("A_total"), col("A_video_play")))
//      .withColumn("F_Percentage", percentageUDF(col("F_total"), col("F_video_play")))
//      .withColumn("NA_percentage", percentageUDF(col("NA_total"), col("NA_video_play")))
//      .withColumn("P_percentage", percentageUDF(col("P_total"), col("P_video_play")))
//
//    val selectDF = perDF.select("concept_code", "concept_name", "A_total", "A_video_play", "A_percentage", "P_total", "P_video_play", "P_percentage", "F_total", "F_video_play", "F_Percentage", "NA_total", "NA_video_play", "NA_percentage")
//    val analysisOutputPrefixPath = "s3a://dsl-datastore/data/video-analysis/output"
//    selectDF.coalesce(1).write.mode(SaveMode.Overwrite).option("header", "true").csv(analysisOutputPrefixPath + "/pc-learningmap-videolikelyhood/month/2018/10")
//  }
}
