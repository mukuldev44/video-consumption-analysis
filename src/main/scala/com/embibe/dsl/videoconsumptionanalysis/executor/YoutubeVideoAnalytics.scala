package com.embibe.dsl.videoconsumptionanalysis.executor

import org.apache.spark.sql.{DataFrame, Dataset, SaveMode, SparkSession}
import org.apache.spark.sql.functions._

object YoutubeVideoAnalytics {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder
      .master("local[*]")
      .appName(getClass.getSimpleName)
      .enableHiveSupport()
      .getOrCreate()
    import spark.implicits._

    val youtubeMetaDF = spark
      .read
      .json("s3a://dsl-datastore/data/video-analysis/youtube/video_details_cleaned.json")
      .dropDuplicates(Seq("url"))

    val clickstreamDS = spark.read.parquet("s3a://dsl-datastore/data/video-analysis/clickstream-learningmap/year/2018")
      .as[UrlMappedSchema]
//      .filter(_.month == 10)

    val clickstreamVideoPlayDS = clickstreamDS
      .filter(trim(col("event_name")) === lit("play/pause video") && trim(col("video_state")) === lit("play"))

    val clickstreamVideoPlayYoutbeMetaDF = clickstreamVideoPlayDS.join(broadcast(youtubeMetaDF),
      trim(clickstreamVideoPlayDS("video_url")) === trim(youtubeMetaDF("url"))
    ).drop(youtubeMetaDF("url"))

    val analysisOutputPrefixPath = "s3a://dsl-datastore/data/video-analysis/output"

    // Top Youtube Video
    val topYoutubeVideoDF = getTopDF(clickstreamVideoPlayDS.toDF(), List("video_url"))

    topYoutubeVideoDF
      .coalesce(1)
      .write
      .option("header", "true")
      .mode(SaveMode.Overwrite)
      .csv(analysisOutputPrefixPath + "/top-youtube-video/year/2018")
    spark.read.option("header", "true").csv(analysisOutputPrefixPath + "/top-youtube-video/year/2018").limit(1000)
      .coalesce(1)
      .write
      .option("header", "true")
      .mode(SaveMode.Overwrite)
      .csv(analysisOutputPrefixPath + "/top-youtube-video/top-1000/year/2018")

    // Top Category
    val topCategoryDF = getTopDF(youtubeMetaDF, List("category_name"))
    topCategoryDF
      .coalesce(1)
      .write.option("header", "true")
      .mode(SaveMode.Overwrite)
      .csv(analysisOutputPrefixPath + "/top-youtube-category/year/2018")

    // Top Channel
    val topChannelDF = getTopDF(youtubeMetaDF, List("channel_title"))
    topChannelDF
      .coalesce(1)
      .write.option("header", "true")
      .mode(SaveMode.Overwrite)
      .csv(analysisOutputPrefixPath + "/top-youtube-channel/year/2018")

    // Top Tags
    val tagsDF = youtubeMetaDF
      .select("tags")
      .withColumn("tags", split(col("tags"), ","))
      .withColumn("tags_explode", explode(col("tags")))
      .withColumn("tag", trim(lower(col("tags_explode"))))
      .drop("tags")
      .drop("tags_explode")
    val topTagsDF = getTopDF(tagsDF, List("tag"))
    topTagsDF
      .coalesce(1)
      .write.option("header", "true")
      .mode(SaveMode.Overwrite)
      .csv(analysisOutputPrefixPath + "/top-youtube-tag/year/2018")
  }

  def getTopDF(df: DataFrame, groupColumnList: List[String]) = {
    df
      .groupBy(groupColumnList.head, groupColumnList.tail:_ *)
      .agg(count("*").as("count"))
      .withColumn("percentage", (col("count") * 100)/sum("count").over())
      .orderBy(desc("percentage"))
  }
}
