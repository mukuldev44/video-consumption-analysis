package com.embibe.dsl.videoconsumptionanalysis.executor

import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.ml.feature.Bucketizer
import eu.bitwalker.useragentutils.UserAgent
import org.apache.spark.sql.types._
import org.apache.spark.ml.stat.Correlation
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.linalg.Matrix

object UserVideoDurationAnalytics {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder
      .master("yarn-client")
      .appName(getClass.getSimpleName)
      .enableHiveSupport()
      .getOrCreate()
    import spark.implicits._

    val userGovDF = spark.read.parquet("s3a://dsl-datastore/data/video-analysis/user-gov/year/2018/")
      .as[GovInputSchema]
      .filter(x => x.month == 3 || x.month == 4 || x.month == 5)
      .withColumn("pc", when(col("pc").isNull, lit("NA")).otherwise(col("pc")))

    val videoClickStreamDF = spark.read.parquet("s3a://dsl-datastore/data/video-analysis/clickstream-learningmap/year/2018")
      .as[UrlMappedSchema]
      .filter(x => x.event_name.trim == "play/pause video" && x.video_state.trim == "pause")
      .filter(x => x.month == 3 || x.month == 4 || x.month == 5)

    val lastVideoPauseClickstreamDF = getLastClickStreamSessionDS(videoClickStreamDF)

    val joinDF = userGovDF.join(lastVideoPauseClickstreamDF,
      userGovDF("user_id") === lastVideoPauseClickstreamDF("user_id") &&
        userGovDF("year") === lastVideoPauseClickstreamDF("year") &&
        userGovDF("month") === lastVideoPauseClickstreamDF("month") &&
        userGovDF("month_day") === lastVideoPauseClickstreamDF("month_day"))
      .drop(userGovDF("user_id"))
      .drop(userGovDF("started_at"))
      .drop(userGovDF("date"))
      .drop(userGovDF("year"))
      .drop(userGovDF("month"))
      .drop(userGovDF("week"))
      .drop(userGovDF("month_day"))

    val thresholds30 = Double.MinValue +: (0.0 until 930.0 by 30).toArray :+ Double.MaxValue

    val bucketizer = new Bucketizer()
      .setInputCol("video_time_position")
      .setOutputCol("video_time_position_bucket_id")
      .setSplits(thresholds30)
    val videoClickStreamBucketDF = bucketizer.transform(lastVideoPauseClickstreamDF)
    val deviceVideoClickStreamBucketDF = videoClickStreamBucketDF.withColumn("device_type", deviceTypeUDF(col("user_agent")))
    val joinBucketDF = bucketizer.transform(joinDF)

    val completeAggDF = getAggDF(videoClickStreamBucketDF, null, thresholds30)
    val pcAggDF = getAggDF(joinBucketDF, "pc", thresholds30)
    val ucAggDF = getAggDF(joinBucketDF, "current_uc", thresholds30)
    val deviceAggDF = getAggDF(deviceVideoClickStreamBucketDF, "device_type", thresholds30)
    val analysisOutputPrefixPath = "s3a://dsl-datastore/data/video-analysis/output"

    writeCSV(completeAggDF, analysisOutputPrefixPath + "/video-duration-histogram/year/2018")
    writeCSV(pcAggDF, analysisOutputPrefixPath + "/pc-video-duration-histogram/year/2018")
    writeCSV(ucAggDF, analysisOutputPrefixPath + "/uc-video-duration-histogram/year/2018")
    writeCSV(deviceAggDF, analysisOutputPrefixPath + "/device-video-duration-histogram/year/2018")

    // correlation analysis
    correlationAnalysis(joinDF, spark)
  }

  def correlationAnalysis(joinDF: DataFrame, spark: SparkSession) = {
    import spark.implicits._

    val selectDF = joinDF.select("user_id", "url", "session_id", "video_url", "timestamp", "video_time_position", "total_clickstream_sessions", "total_learn_browses", "total_learn_searches", "total_learn_sessions", "total_looked", "total_practice_attempts", "total_practice_corrects", "total_practice_sessions", "total_server_sessions", "total_test_attempts", "total_test_corrects", "total_test_sessions", "total_valid_chapter_tests", "total_valid_full_tests", "total_valid_other_tests", "total_valid_tests", "learn_timespent", "overall_timespent", "practice_timespent", "test_timespent")
    selectDF.write.parquet("s3a://dsl-datastore/data/video-analysis/persist/video-last-pause-attempt/parquet/month/3-4-5")

    val selectParDF = spark.read.parquet("s3a://dsl-datastore/data/video-analysis/persist/video-last-pause-attempt/parquet/month/3-4-5")

    val otherColList = Array("total_clickstream_sessions", "total_learn_browses", "total_learn_searches", "total_learn_sessions", "total_looked", "total_practice_attempts", "total_practice_corrects", "total_practice_sessions", "total_server_sessions", "total_test_attempts", "total_test_corrects", "total_test_sessions", "total_valid_chapter_tests", "total_valid_full_tests", "total_valid_other_tests", "total_valid_tests", "learn_timespent", "overall_timespent", "practice_timespent", "test_timespent")

    val featureCastDF = otherColList.foldLeft(selectParDF){ (accDF, colName) =>
      accDF.withColumn(colName, col(colName).cast(LongType))
    }

    val featureColList = "video_time_position" +: otherColList

    val assembler = new VectorAssembler().setInputCols(featureColList).setOutputCol("corr_features")

    val df_vector = assembler.transform(featureCastDF).select("corr_features", featureColList: _*)

    val pearsonMatrix = Correlation.corr(df_vector, "corr_features", "pearson")
    val Row(pearsonCorrMatrix: Matrix) = pearsonMatrix.head()

    val colNamePairs = featureColList.flatMap(c1 => featureColList.map(c2 => (c1, c2)))

    val triplesList_pearson = colNamePairs.zip(pearsonCorrMatrix.toArray)
      .filterNot(p => p._1._1 >= p._1._2 )
      .map(r => (r._1._1, r._1._2, r._2))

    val pearsonCorrDF = spark.sparkContext.parallelize(triplesList_pearson).toDF("item_from", "item_to", "Correlation")
    pearsonCorrDF.filter(col("item_from") === "video_time_position" || col("item_to") === "video_time_position").show(100, false)

    val spearmanMatrix = Correlation.corr(df_vector, "corr_features", "spearman")
    val Row(spearamanCorrMatrix: Matrix) = pearsonMatrix.head()

    val triplesList_spearman = colNamePairs.zip(spearamanCorrMatrix.toArray)
      .filterNot(p => p._1._1 >= p._1._2 )
      .map(r => (r._1._1, r._1._2, r._2))

    val spearmanCorrDF = spark.sparkContext.parallelize(triplesList_pearson).toDF("item_from", "item_to", "Correlation")
    spearmanCorrDF.filter(col("item_from") === "video_time_position" || col("item_to") === "video_time_position").show(100, false)
  }

  def writeCSV(df: DataFrame, path: String) = {
    df
      .coalesce(1)
      .write.mode(SaveMode.Overwrite)
      .option("header", "true")
      .csv(path)
  }

  def getAggDF(df: DataFrame, pivotColumn: String, thresholds: Array[Double] ) = {
    def rangeViewUDF(thresholdArray: Array[Double]) = udf{(id: Double) =>
      val _1 = thresholdArray(id.toInt)
      val _2 = thresholdArray(id.toInt + 1)

      if (_1 == Double.MinValue) {
        "infinity" + " to " + _2
      }
      else if (_2 == Double.MaxValue) {
        _1 + " to " + "infinity"
      }
      else {
        _1 + " to " + _2
      }
    }
    if (pivotColumn == null) {
      df
        .groupBy("video_time_position_bucket_id")
        .agg(count("*").as("frequency"))
        .withColumn("bucket_range", rangeViewUDF(thresholds)(col("video_time_position_bucket_id")))
        .orderBy("video_time_position_bucket_id")
        .drop("video_time_position_bucket_id")
    } else {
      df
        .groupBy("video_time_position_bucket_id")
        .pivot(pivotColumn)
        .agg(count("*").as("frequency"))
        .withColumn("bucket_range", rangeViewUDF(thresholds)(col("video_time_position_bucket_id")))
        .orderBy("video_time_position_bucket_id")
        .drop("video_time_position_bucket_id")
    }
  }

  val deviceTypeUDF = udf{(userAgent: String) =>
    val deviceType = UserAgent.parseUserAgentString(userAgent).getOperatingSystem.getDeviceType
    deviceType.toString
  }

  def getLastClickStreamSessionDS(ds: Dataset[UrlMappedSchema]) = {
    import ds.sparkSession.implicits._
    val lastClickStreamSessionDS = ds
      .groupByKey(x => (x.user_id, x.url, x.session_id, x.video_url))
      .reduceGroups((x, y) => if(x.timestamp > y.timestamp) x else y)
      .map(_._2)
    lastClickStreamSessionDS
  }
}
