package com.embibe.dsl.videoconsumptionanalysis.executor

import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.functions._

object UserAnalytics {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder
      .master("yarn-client")
      .appName(getClass.getSimpleName)
      .enableHiveSupport()
      .getOrCreate()
    import spark.implicits._

    val userGovDF = spark.read.parquet("s3a://dsl-datastore/data/video-analysis/user-gov/year/2018/")
      .as[GovInputSchema]
//      .filter(_.month == 4)
      .withColumn("pc", when(col("pc").isNull, lit("NA")).otherwise(col("pc")))

    val videoClickStreamDF = spark.read.parquet("s3a://dsl-datastore/data/video-analysis/clickstream-learningmap/year/2018")
      .as[UrlMappedSchema]
      .filter(_.event_name.trim == "play/pause video")
//      .filter(_.month == 4)

    val joinDF = userGovDF.join(videoClickStreamDF,
      userGovDF("user_id") === videoClickStreamDF("user_id") && userGovDF("month_day") === videoClickStreamDF("month_day"))
      .drop(userGovDF("user_id"))
      .drop(userGovDF("started_at"))
      .drop(userGovDF("date"))
      .drop(userGovDF("year"))
      .drop(userGovDF("month"))
      .drop(userGovDF("week"))
      .drop(userGovDF("month_day"))

    val joinDF1 = joinDF
      .filter(col("video_state") === "play")

    val analysisOutputPrefixPath = "s3a://dsl-datastore/data/video-analysis/output"
    val pcVideoPlayDF = getPerformanceCohortVideoPlays(joinDF1).na.fill(0)
    pcVideoPlayDF
      .coalesce(1)
      .write
      .mode(SaveMode.Overwrite)
      .option("header", "true")
      .csv(analysisOutputPrefixPath + "/pc-hour-video-play/year/2018")
  }

  def getPerformanceCohortVideoPlays(joinDF: DataFrame) = {
    joinDF
      .groupBy("hour")
      .pivot("pc")
      .agg(count("*"))
      .orderBy("hour")
  }
}
