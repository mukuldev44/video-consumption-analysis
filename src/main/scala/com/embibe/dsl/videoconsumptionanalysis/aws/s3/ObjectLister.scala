package com.embibe.dsl.videoconsumptionanalysis.aws.s3

import com.amazonaws.services.s3.AmazonS3ClientBuilder
import scala.collection.JavaConverters._

object ObjectLister {
  // Building S3 Client
  private[aws] val s3Client = AmazonS3ClientBuilder.standard.build

  /** Returns List of object-paths for specified `bucketName` and `dirPrefix`
    *
    * @param bucketName   name of required bucket
    * @param dirPrefix    prefix of required directory
    * @return             List of object-paths for specified `bucketName` and `dirPrefix`
    */
  def getS3Objects(bucketName: String, dirPrefix: String): List[String] = {
    val bucketObjects = if(dirPrefix == null || dirPrefix.isEmpty) {
      s3Client.listObjects(bucketName)
    }
    else {
      s3Client.listObjects(bucketName, dirPrefix)
    }
    val objectSummaryList = bucketObjects.getObjectSummaries.asScala.toList
    val objectKeyList = objectSummaryList.map(_.getKey)

    objectKeyList
  }

  /** Returns List of object-paths for specified `bucketName`
    *
    * @param bucketName   name of required bucket
    * @return             List of object-paths for specified `bucketName`
    */
  def getS3Objects(bucketName: String): List[String] = getS3Objects(bucketName, null)
}
